//index.js
//获取应用实例
var app = getApp()
Page({
    data: {
        array: ['iPhone5s', 'iPhone6/7', 'iPhone6/7P'],
        unit: ["px", "dp", "rpx"],
        uindex: 0,
        index: 0,
        rate: [['1', '2', '2.34'], ['1', '2', '2'], ['1', '3', '1']]
    },

    Picker: function (e) {
        console.log(e.detail.value)
        this.setData({
            index: e.detail.value
        })
    },

    UintPicker: function (e) {
        console.log(e.detail.value)
        this.setData({
            uindex: e.detail.value
        })
    },

    Valueinput: function (e) {
        if (this.data.uindex == 0) {
            this.setData({
                PXValue: e.detail.value,
                DPValue: e.detail.value * this.data.rate[this.data.index][1],
                RPXValue: e.detail.value * this.data.rate[this.data.index][2]
            });
        } else if (this.data.uindex == 1) {
            this.setData({
                PXValue: e.detail.value / this.data.rate[this.data.index][1],
                DPValue: e.detail.value,
                RPXValue: e.detail.value / this.data.rate[this.data.index][1] * this.data.rate[this.data.index][2]
            });
        } else if (this.data.uindex == 2) {
            this.setData({
                PXValue: e.detail.value / this.data.rate[this.data.index][2],
                DPValue: e.detail.value / this.data.rate[this.data.index][2] * this.data.rate[this.data.index][1],
                RPXValue: e.detail.value
            });
        }
    }
})
